<?php

namespace App\Service;

use App\Entity\Task;

interface HandlerInterface
{
    public function handle(Task $task, array $configs): void;
}
