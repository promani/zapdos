<?php

namespace App\Service;

use App\Entity\Config;
use App\Entity\Task;
use App\Util\Common;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Contracts\HttpClient\Exception\HttpExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class QueryHandler implements HandlerInterface
{
    public function __construct(private ManagerRegistry $registry)
    {
    }

    public function handle(Task $task, $configs): void
    {
        $data = $task->getData();
        $result = $configs['template'];
        Common::replaceRecursive($data, $result);
        $task->setResult($result);

        $em = $this->registry->getManager($configs['connection']);
        // some kind of execute query

        $this->registry->getManager()->persist($task);
        $this->registry->getManager()->flush();
    }

}
