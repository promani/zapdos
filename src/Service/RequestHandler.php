<?php

namespace App\Service;

use App\Entity\Config;
use App\Entity\Task;
use App\Util\Common;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Contracts\HttpClient\Exception\HttpExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class RequestHandler implements HandlerInterface
{
    public function __construct(private HttpClientInterface $client)
    {
    }

    public function handle(Task $task, $configs): void
    {
        $data = json_decode(json_encode($task->getData()), true);
        $body = $configs['body'] ?? [];
        Common::replaceRecursive($data, $body);
        $url = $configs['url'];
        Common::replaceRecursive($data, $url);
        $headers = $configs['headers'] ?? [];
        Common::replaceRecursive([...$configs, ...$data], $headers);
        $query = $configs['query'] ?? [];
        Common::replaceRecursive([...$configs, ...$data], $query);
        // TODO: Add previous request result.

        try {
            $result = $this->client->request($configs['method'], $url, [
                'body' => $body,
                'headers' => $headers,
                'query' => $query,
                'timeout' => 2.5,
                'verify_host' => false,
                'verify_peer' => false
            ]);

            $task->setStatus('SUCCESS');
            $task->addLogs('REQUEST RESPONSE WITH STATUS CODE '. $result->getStatusCode() . ' AT ' . date_create()->format('Y-m-d H:i:s'));
            $task->setResult($result->getContent());
        } catch (HttpExceptionInterface $e) {
            $task->setStatus(Task::STATUS_FAILURE);
            $task->addLogs($e->getMessage());
        } catch (\Throwable $throwable) {
            $task->setStatus(Task::STATUS_FAILURE);
            $task->addLogs($throwable->getMessage());
        }
    }

}
