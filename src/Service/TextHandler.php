<?php

namespace App\Service;

use App\Entity\Task;
use App\Util\Common;

class TextHandler implements HandlerInterface
{
    public function handle(Task $task, $configs): void
    {
        $data = $task->getData();
        $result = $configs['template'];
        Common::replaceRecursive($data, $result);
        $task->setResult($result);
    }
}
