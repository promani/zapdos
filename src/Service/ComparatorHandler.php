<?php

namespace App\Service;

use App\Entity\Task;
use Doctrine\Persistence\ManagerRegistry;

class ComparatorHandler implements HandlerInterface
{
    public function __construct(private ManagerRegistry $registry)
    {
    }

    public function handle(Task $task, $configs): void
    {
        if ($task->getAction()->getPattern()) {
            $task->setResult($task->getData());
        }
    }
}
