<?php

namespace App\Service;

use App\Entity\Action;
use App\Entity\Config;
use App\Entity\Task;
use App\Message\TaskMessage;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class TaskHandler
{
    protected RequestHandler $requestHandler;

    public function __construct(
        HttpClientInterface $client,
        private ManagerRegistry     $registry,
        private MessageBusInterface $bus,
        private TextHandler         $textHandler,
        private QueryHandler        $queryHandler,
        private ComparatorHandler   $comparationHandler,
    ) {
        $this->requestHandler = new RequestHandler($client);
    }

    public function handle(Task $task): void
    {
        $type = $task->getAction()->getType();
        $action = $task->getAction();
        $configs = $action->getConfigs();
        $globals = $this->registry->getRepository(Config::class)->findAll();
        foreach ($globals as $global) {
            $configs[$global->getId()] = $global->getValue();
        }

        if ($previous = $task->getPrevious()) {
            $task->mergeData($previous->getData());
        }

        switch ($type) {
            case Action::ACTION_TYPES_REQUEST:
                $this->requestHandler->handle($task, $configs);
                break;
            case Action::ACTION_TYPES_TEXT:
                $this->textHandler->handle($task, $configs);
                break;
            case Action::ACTION_TYPES_QUERY:
                $this->queryHandler->handle($task, $configs);
                break;
            case Action::ACTION_TYPES_COMPARATION:
                $this->comparationHandler->handle($task, $configs);
                break;
            default:
                throw new \Exception("No handler available for $type.");
        }

        $em = $this->registry->getManager();
        if ($task->getStatus() == Task::STATUS_SUCCESS && $task->getResult()) {
            $task->addLogs($task->getResult());
            if ($patterns = $task->getAction()->getPattern()) {
                $result = [];
                foreach (explode(',', $patterns) as $pattern) {
                    try {
                        $expressionLanguage = new ExpressionLanguage();
                        $result[] = $expressionLanguage->evaluate($pattern, json_decode($task->getResult(), true));
                    } catch (\Throwable $throwable) {
                        $task->setStatus(Task::STATUS_FAILURE);
                        $task->addLogs('FAILED TO APPLY THE PATTERN');
                    }
                }
                $task->setResult(json_encode($result));
            }
        }
        $em->persist($task);
        $em->flush();

        if ($task->getStatus() == Task::STATUS_FAILURE) {
            throw new \Exception('UNABLE TO PROCESS: ' . $task->getResult());
        }

        // if ($task->getStatus() == Task::STATUS_SUCCESS && $task->getNext()) {
        //    $this->bus->dispatch((new TaskMessage($task->getNext()->getId())));
        //}
    }
}
