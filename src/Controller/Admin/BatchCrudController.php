<?php

namespace App\Controller\Admin;

use App\Entity\Batch;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class BatchCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Batch::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        $processAction = Action::new('batchProcess', 'Process')
            ->linkToRoute('batches_process', fn (Batch $batch) => ['id' => $batch->getId()]);
        $resetAction = Action::new('batchReset', 'Reset')
            ->linkToRoute('batches_reset', fn (Batch $batch) => ['id' => $batch->getId()]);
        $retryAction = Action::new('batchRetry', 'Retry')
            ->linkToRoute('batches_retry', fn (Batch $batch) => ['id' => $batch->getId()]);
        $resultAction = Action::new('batchResult', 'Result')
            ->linkToRoute('batches_result', fn (Batch $batch) => ['id' => $batch->getId()]);
        return $actions
            ->add(Crud::PAGE_INDEX, $processAction)
            ->add(Crud::PAGE_INDEX, $resetAction)
            ->add(Crud::PAGE_INDEX, $retryAction)
            ->add(Crud::PAGE_INDEX, $resultAction);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
            AssociationField::new('actions'),
            ImageField::new('file')->setUploadDir('public/data/')->hideOnIndex(),
            AssociationField::new('tasks')->hideOnForm()
        ];
    }

}
