<?php

namespace App\Controller\Admin;

use App\Entity\Task;
use App\Form\JsonType;
use App\Service\TaskHandler;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CodeEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TaskCrudController extends AbstractCrudController
{

    public static function getEntityFqcn(): string
    {
        return Task::class;
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('id')
            ->add('status')
            ->add('batch');
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        return parent::createIndexQueryBuilder($searchDto, $entityDto, $fields, $filters)->addOrderBy('entity.id', 'DESC');
    }

    public function configureActions(Actions $actions): Actions
    {
        $processAction = Action::new('taskProcess', 'Process')
            ->linkToCrudAction('process');
        $resultAction = Action::new('taskResult', 'Result')
            ->linkToCrudAction('result');
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_INDEX, $processAction)
            ->add(Crud::PAGE_INDEX, $resultAction);
    }

    public function result(AdminContext $context): Response
    {
        $entity = $context->getEntity()->getInstance();
        return $this->render('/task/result.html.twig', ['task' => $entity]);
    }

    public function process(AdminContext $context, Request $request, TaskHandler $handler): Response
    {
        $task = $context->getEntity()->getInstance();
        try {
            $handler->handle($task);
        } catch (\Exception $exception) {
            $request->getSession()->getFlashBag()->add('error', $exception->getMessage());
        } finally {
            $route = $request->headers->get('referer');
            return $this->redirect($route);
        }
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            AssociationField::new('action'),
            AssociationField::new('batch')->hideOnForm(),
            CodeEditorField::new('data')
                ->setFormType(JsonType::class)
                ->setNumOfRows(12)
                ->formatValue(static fn ($data) => json_encode($data))
                ->hideOnDetail(),
            TextField::new('result')->hideOnForm(),
            AssociationField::new('next'),
        ];
    }

}
