<?php

namespace App\Controller\Admin;

use App\Entity\Action;
use App\Entity\Batch;
use App\Entity\Config;
use App\Entity\Schedule;
use App\Entity\Task;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/')]
class DashboardController extends AbstractDashboardController
{
    #[Route('/', name: 'dashboard')]
    public function index(): Response
    {
        $routeBuilder = $this->get(AdminUrlGenerator::class);

        return $this->redirect($routeBuilder->setController(TaskCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('<img class="d-none d-lg-block" src="/img/zapdos">')
            ->setFaviconPath('/img/zapdos');
    }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::linkToCrud('Batches', 'fa fa-box', Batch::class),
            MenuItem::linkToCrud('Tasks', 'fa fa-clipboard-check', Task::class),
            MenuItem::linkToCrud('Actions', 'fa fa-play', Action::class),
            MenuItem::linkToCrud('Schedules', 'fa fa-clock', Schedule::class),
            MenuItem::linkToCrud('Configs', 'fa fa-cogs', Config::class),
        ];
    }
}
