<?php

namespace App\Controller\Admin;

use App\Entity\Action;
use App\Form\JsonType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action as EasyAdminAction;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CodeEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ActionCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Action::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        $cloneAction = EasyAdminAction::new('cloneAction', 'Clone')
            ->linkToRoute('actions_clone', fn (Action $action) => ['id' => $action->getId()]);
        return $actions
            ->add(Crud::PAGE_INDEX, $cloneAction);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
            ChoiceField::new('type')->setChoices(array_combine(Action::ACTION_TYPES, Action::ACTION_TYPES)),
            TextField::new('pattern'),
            CodeEditorField::new('configs')
                ->setFormType(JsonType::class)->setNumOfRows(12)
                ->hideOnIndex()
                ->hideOnDetail(),
        ];
    }

}
