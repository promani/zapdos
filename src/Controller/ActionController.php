<?php

namespace App\Controller;

use App\Entity\Action;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/actions', name: 'actions_')]
class ActionController extends AbstractController
{
    #[Route('/{id}/clone', name: 'clone')]
    public function process(Action $action, Request $request, ManagerRegistry $registry): Response
    {
        $newAction = clone $action;
        $newAction->setName($action->getName() . ' (copy)');
        $registry->getManager()->persist($newAction);
        $registry->getManager()->flush();

        $route = $request->headers->get('referer');
        return $this->redirect($route);
    }

}
