<?php

namespace App\Controller;

use App\Entity\Batch;
use App\Entity\Task;
use App\Message\BatchMessage;
use App\Message\TaskMessage;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

const BATCH_SIZE = 100;

#[Route('/batches', name: 'batches_')]
class BatchController extends AbstractController
{
    #[Route('/{id}/result', name: 'result')]
    public function result(Batch $batch, ManagerRegistry $registry): Response
    {
        $taskRepo = $registry->getManager()->getRepository(Task::class);
        $success = $taskRepo->count(['batch' => $batch, 'status' => Task::STATUS_SUCCESS]);
        $failure = $taskRepo->count(['batch' => $batch, 'status' => Task::STATUS_FAILURE]);
        return $this->render('/batch/result.html.twig', [
            'batch' => $batch,
            'success' => $success,
            'failure' => $failure
        ]);
    }

    #[Route('/{id}/process', name: 'process')]
    public function process(Batch $batch, Request $request, ParameterBagInterface $bag, ManagerRegistry $registry, MessageBusInterface $bus): Response
    {
        $offset = $batch->getTasks()->count();
        $em = $registry->getManager();
        $handle = fopen($bag->get('kernel.project_dir') . "/public/data/" . $batch->getFile(), "r");
        if ($handle !== false) {
            $count = 0;
            $headers = [];
            while (($data = fgetcsv($handle, 0, ",")) !== false) {
                $count++;
                if ($count == 1) {
                    $headers = $data;
                    continue;
                }
                if ($count < $offset) {
                    continue;
                }
                $previous = null;
                foreach ($batch->getActions() as $action) {
                    $task = new Task();
                    $task->setBatch($batch);
                    $task->setAction($action);
                    $result = [];
                    for ($i = 0; $i < count($headers); $i++) {
                        $result[$headers[$i]] = $data[$i];
                    }
                    $task->setData($result);
                    if ($previous) {
                        $task->setPrevious($previous);
                    }
                    $previous = $task;
                    $em->persist($task);
                }
                if ($count % BATCH_SIZE == 0) {
                    $em->flush();
                    $em->clear();
                    $batch = $em->getRepository(Batch::class)->find($batch->getId());
                }
            }
            fclose($handle);
        }
        $em->flush();
        $bus->dispatch(new BatchMessage($batch->getId()));

        $route = $request->headers->get('referer');
        return $this->redirect($route);
    }

    #[Route('/{id}/retry', name: 'retry')]
    public function retry(Batch $batch, Request $request, ManagerRegistry $registry, MessageBusInterface $bus): Response
    {
        $em = $registry->getManager();
        /** @var Task $task */
        foreach ($batch->getTasks() as $task) {
            if ($task->getStatus() !== Task::STATUS_SUCCESS) {
                $task->setStatus(Task::STATUS_CREATED);
                $task->setResult(null);
                $bus->dispatch(new TaskMessage($task->getId()));
            }
        }
        $em->flush();

        $route = $request->headers->get('referer');
        return $this->redirect($route);
    }

    #[Route('/{id}/reset', name: 'reset')]
    public function reset(Batch $batch, Request $request, ManagerRegistry $registry, MessageBusInterface $bus): Response
    {
        $em = $registry->getManager();
        /** @var Task $task */
        foreach ($batch->getTasks() as $task) {
            $task->setStatus(Task::STATUS_CREATED);
            $task->setResult(null);
            $bus->dispatch(new TaskMessage($task->getId()));
        }
        $em->flush();

        $route = $request->headers->get('referer');
        return $this->redirect($route);
    }

}
