<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Action
{
    const ACTION_TYPES_REQUEST = 'REQUEST';
    const ACTION_TYPES_QUERY = 'QUERY';
    const ACTION_TYPES_TEXT = 'TEXT';
    const ACTION_TYPES_COMPARATION = 'COMPARATION';

    const ACTION_TYPES = [
        self::ACTION_TYPES_REQUEST,
        self::ACTION_TYPES_QUERY,
        self::ACTION_TYPES_TEXT
    ];

    #[ORM\Column(type: 'integer')]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private int $id;

    #[ORM\Column(type: 'string')]
    private string $name = '';

    #[ORM\Column(type: 'string')]
    private string $type = self::ACTION_TYPES_REQUEST;

    #[ORM\Column(type: 'json', nullable: true)]
    private $configs;

    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $pattern = null;

    #[ORM\ManyToOne(targetEntity: Action::class)]
    private ?Action $fallback = null;
    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getConfigs()
    {
        return $this->configs;
    }

    public function setConfigs($configs): void
    {
        $this->configs = $configs;
    }

    public function getPattern(): ?string
    {
        return $this->pattern;
    }

    public function setPattern(?string $pattern):void
    {
        $this->pattern = $pattern;
    }

    public function getFallback(): ?Action
    {
        return $this->fallback;
    }
    public function setFallback(?Action $fallback):void
    {
        $this->fallback = $fallback;
    }

    public function __toString(): string
    {
        return '[' . $this->type . '] ' . $this->name;
    }

}
