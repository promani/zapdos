<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Task
{
    const STATUS_CREATED = 'CREATED';
    const STATUS_SUCCESS = 'SUCCESS';
    const STATUS_FAILURE = 'FAILURE';

    const ACTION_TYPES = [
        self::STATUS_CREATED,
        self::STATUS_SUCCESS,
        self::STATUS_FAILURE
    ];

    #[ORM\Column(type: 'integer')]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private int $id;

    #[ORM\ManyToOne(targetEntity: Action::class)]
    private Action $action;

    #[ORM\ManyToOne(cascade: ["persist"], targetEntity: Batch::class, inversedBy: 'tasks')]
    private ?Batch $batch = null;

    #[ORM\Column(type: 'json', nullable: true)]
    private $data;

    #[ORM\Column(type: 'string', nullable: true)]
    private string $status = 'CREATED';

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $result = '';

    /** @var Task|null */
    #[ORM\OneToOne(mappedBy: 'previous', targetEntity: Task::class)]
    private Task|null $next;

    /** @var Task */
    #[ORM\OneToOne(inversedBy: 'next', targetEntity: Task::class)]
    #[ORM\JoinColumn(onDelete: "CASCADE")]
    private Task|null $previous;

    #[ORM\Column(type: 'array', nullable: true)]
    private ?array $logs;

    public function getId(): int
    {
        return $this->id;
    }

    public function getBatch(): ?Batch
    {
        return $this->batch;
    }

    public function setBatch(Batch $batch): void
    {
        $this->batch = $batch;
    }

    public function getAction(): Action
    {
        return $this->action;
    }

    public function setAction(Action $action): void
    {
        $this->action = $action;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data): void
    {
        $this->data = $data;
    }

    public function mergeData($data): void
    {
        $this->data = (object) array_merge((array) $this->data, (array) $data);
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    public function getResult(): ?string
    {
        return $this->result;
    }

    public function isObject(): bool
    {
        return !!json_decode($this->result);
    }

    public function getResultParsed(): mixed
    {
        if (!$this->result) {
            return '';
        }
        return json_decode($this->result, true) ?? 'UNABLE TO PARSE';
    }

    public function setResult(?string $result): void
    {
        $this->result = $result;
    }

    public function getNext(): Task|null
    {
        return $this->next;
    }

    public function setNext(Task $next): void
    {
        $this->next = $next;
    }

    public function getPrevious(): Task|null
    {
        return $this->previous;
    }

    public function setPrevious(Task|null $previous): void
    {
        $this->previous = $previous;
    }
    
    public function getLogs(): ?array
    {
        return $this->logs;
    }

    public function addLogs($log):void
    {
        $this->logs[] = $log;
    }

    public function setLogs(?array $logs):void
    {
        $this->logs = $logs;
    }

    public function getIcon(): string
    {
        switch ($this->status) {
            case self::STATUS_SUCCESS:
                return 'check-circle';
            case self::STATUS_FAILURE:
                return 'exclamation-circle';
            default:
                return 'hourglass-start';
        }
    }

    public function getColor(): string
    {
        switch ($this->status) {
            case self::STATUS_FAILURE:
                return 'danger';
            default:
                return '';
        }
    }

    public function __toString(): string
    {
        return $this->id;
    }

}
