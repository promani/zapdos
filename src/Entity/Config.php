<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ApiResource(itemOperations: ['get'])]
class Config
{
    #[ORM\Column(type: 'boolean', nullable: true, options: ['default' => 1])]
    protected ?bool $active = true;
    #[ORM\Column(type: 'string')]
    #[ORM\Id]
    private ?string $id;
    #[ORM\Column(type: 'string', nullable: true)]
    private string $value = '';

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(?string $id): void
    {
        $this->id = $id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): void
    {
        $this->value = $value;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): void
    {
        $this->active = $active;
    }

    public function __toString()
    {
        return $this->id;
    }
}
