<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Schedule
{
    #[ORM\Column(type: 'integer')]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private int $id;

    #[ORM\ManyToOne(targetEntity: Action::class)]
    private Action $action;

    #[ORM\Column(type: 'json', nullable: true)]
    private $data = '';

    #[ORM\Column(type: 'date', nullable: true)]
    private \DateTime $date;

    #[ORM\Column(type: 'string', nullable: true)]
    private string $repeat = '';

    public function getId(): int
    {
        return $this->id;
    }

    public function getAction(): Action
    {
        return $this->action;
    }

    public function setAction(Action $action): void
    {
        $this->action = $action;
    }

    public function getData(): string
    {
        return $this->data;
    }

    public function setData(string $data): void
    {
        $this->data = $data;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    public function getRepeat(): string
    {
        return $this->repeat;
    }

    public function setRepeat(string $repeat): void
    {
        $this->repeat = $repeat;
    }

}
