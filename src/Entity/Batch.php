<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Batch
{
    #[ORM\Column(type: 'integer')]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private int $id;

    #[ORM\Column(type: 'string', nullable: true)]
    private string $name = '';

    /** @var Collection | Action[] */
    #[ORM\ManyToMany(targetEntity: Action::class)]
    private Collection $actions;

    #[ORM\Column(type: 'integer', nullable: true)]
    private int $delay = 0;

    /** @var Collection | Task[] */
    #[ORM\OneToMany(mappedBy: 'batch', targetEntity: Task::class)]
    private $tasks;

    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $file = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getActions(): Collection
    {
        return $this->actions;
    }
    
    public function setActions(Collection $actions):void
    {
        $this->actions = $actions;
    }

    public function getDelay(): int
    {
        return $this->delay;
    }

    public function setDelay(int $delay): void
    {
        $this->delay = $delay;
    }

    public function getTasks(): Collection|array
    {
        return $this->tasks;
    }

    public function setTasks(Collection|array $tasks): void
    {
        $this->tasks = $tasks;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): void
    {
        $this->file = $file;
    }

    public function __toString() : string
    {
        return $this->name;
    }
}
