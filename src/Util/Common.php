<?php

namespace App\Util;

class Common
{
    public static function replaceRecursive(array $data, &$target)
    {
        $isArray = is_array($target);
        if ($isArray) {
            $target = json_encode($target);
        }
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                self::replaceRecursive($value, $target);
            } else {
                $target = strtr($target, ['{{' . $key . '}}' => $value]);
            }
        }

        $target = strtr($target, ['{{uuid}}' => uuid_create()]);
        $target = strtr($target, ['{{date}}' => date_create()->format('Y-m-d')]);
        $target = strtr($target, ['{{date-1}}' => date_create("yesterday")->format('Y-m-d')]);
        if ($isArray) {
            $target = json_decode($target, true);
        }
    }
}
