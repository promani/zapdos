<?php

namespace App\MessageHandler;

use App\Entity\Batch;
use App\Entity\Task;
use App\Message\BatchMessage;
use App\Message\TaskMessage;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsMessageHandler]
class BatchMessageHandler
{
    public function __construct(private ManagerRegistry $registry, private MessageBusInterface $bus)
    {
    }

    public function __invoke(BatchMessage $message): void
    {
        /** @var Task $task */
        $batch = $this->registry->getRepository(Batch::class)->find($message->getId());
        if (!$batch) {
            return;
        }
        foreach ($batch->getTasks() as $task) {
            if ($task->getStatus() !== Task::STATUS_SUCCESS) {
                $this->bus->dispatch((new TaskMessage($task->getId())));
            }
        }
    }
}
