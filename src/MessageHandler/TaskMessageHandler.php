<?php

namespace App\MessageHandler;

use App\Entity\Task;
use App\Message\TaskMessage;
use App\Service\TaskHandler;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class TaskMessageHandler
{
    public function __construct(private TaskHandler $handler, private ManagerRegistry $registry)
    {
    }

    public function __invoke(TaskMessage $message): void
    {
        /** @var Task $task */
        $task = $this->registry->getRepository(Task::class)->find($message->getId());
        if (!$task) {
            return;
        }
        $this->handler->handle($task);
    }
}
