<?php

namespace App\Message;

class TaskMessage
{
    public function __construct(private int $id)
    {
    }

    public function getId(): int
    {
        return $this->id;
    }

}
