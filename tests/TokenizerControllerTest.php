<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TokenizerControllerTest extends WebTestCase
{
	public function testTokenizerMocked(): void
	{
		$client = static::createClient();

		$client->request('POST', '/tokenizer/receivers/example/deliver', [], [], [], file_get_contents(__DIR__ . '/data/spreedly.deliver.example'));
		$response = $client->getResponse();
		$this->assertResponseIsSuccessful();
		$this->assertEquals(201, $response->getStatusCode());
		$content = json_decode($response->getContent(), true);
		$this->assertEquals(200, $content['amount']);
		$this->assertEquals('VISA', $content['schema']);
	}
	// Missing test for card replacement
}
